package ro.raducirstoiu.http.handlers.connection.keepalive;

import ro.raducirstoiu.http.HeaderValues;
import ro.raducirstoiu.http.Headers;
import ro.raducirstoiu.http.connection.Connection;
import ro.raducirstoiu.http.response.Response;

public abstract class HttpXKeepAliveTemplate implements KeepAliveStrategy {

	@Override
	public void informResponseOfKeepAliveBehavior(Response response) {
		if (response == null) {
			throw new IllegalArgumentException("response cannot be null");
		}
		if (isKeepAlive()) {
			response.setHeader(Headers.CONNECTION,
					HeaderValues.CONNECTION_KEEP_ALIVE);
		} else {
			response.setHeader(Headers.CONNECTION,
					HeaderValues.CONNECTION_CLOSE);
		}
	}

	@Override
	public boolean maybeCloseConnection(Connection connection) {
		if (isKeepAlive()) {
			return false;
		} else {
			if(connection != null){
				connection.close();
			}
			return true;
		}
	}

}
