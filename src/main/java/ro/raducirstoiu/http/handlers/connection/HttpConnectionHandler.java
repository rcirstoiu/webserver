package ro.raducirstoiu.http.handlers.connection;

import java.io.IOException;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.raducirstoiu.http.HeaderValues;
import ro.raducirstoiu.http.Headers;
import ro.raducirstoiu.http.connection.Connection;
import ro.raducirstoiu.http.handlers.connection.keepalive.KeepAliveStrategy;
import ro.raducirstoiu.http.handlers.request.RequestHandler;
import ro.raducirstoiu.http.handlers.request.error.ExceptionHandler;
import ro.raducirstoiu.http.request.HttpRequest;
import ro.raducirstoiu.http.request.Request;
import ro.raducirstoiu.http.response.HttpResponse;
import ro.raducirstoiu.http.response.Response;
import ro.raducirstoiu.http.server.VHostConfig;

public class HttpConnectionHandler implements ConnectionHandler {

	private static final Logger logger = LoggerFactory
			.getLogger(HttpConnectionHandler.class.getSimpleName());
	private final Connection connection;
	private final Map<String, VHostConfig> vhosts;

	public HttpConnectionHandler(Connection connection,
			Map<String, VHostConfig> vhosts) {
		this.connection = connection;
		this.vhosts = vhosts;

	}

	@Override
	public void run() {
		Request httpRequest = new HttpRequest();
		HttpResponse response = null;
		boolean keepAlive = true;

		while (keepAlive) {
			try {
				httpRequest.build(connection.getInputStream());

				String host = httpRequest.getHostName();
				VHostConfig vhost = vhosts.get(host);

				KeepAliveStrategy keepAliveStrategy = vhost.getHttpVersion()
						.getKeepAliveStrategy();
				keepAliveStrategy.readRequestPreference(httpRequest);

				response = new HttpResponse(connection.getOutputStream());
				response.setProtocolVersion(vhost.getHttpVersion());
				keepAliveStrategy.informResponseOfKeepAliveBehavior(response);

				for (RequestHandler requestHandler : vhost.getRequestHandlers()) {
					if (requestHandler.handle(httpRequest, response)) {
						// request was handled, stop trying other
						// requestHandlers
						break;
					}
				}
				keepAlive = !keepAliveStrategy.maybeCloseConnection(connection);
			} catch (IOException e) {
				keepAlive = false;
				logger.error("IOException occured building request", e);
				handleError(connection, e, httpRequest, response);
			} catch (Throwable t) { // catch all -- we must log this, the client
									// must be informed
				keepAlive = false;
				logger.error("Unknown exception occured", t);
				handleError(connection, t, httpRequest, response);
			}
		}
		
		logger.trace("done handling connection");

	}

	private void handleError(Connection connection, Throwable t,
			Request request, Response response) {
		try {
			if (response != null && !response.isCommitted()) {
				response.setHeader(Headers.CONNECTION,
						HeaderValues.CONNECTION_CLOSE);
			}
			new ExceptionHandler(t).handle(request, response);
		} finally {
			if (connection != null) {
				connection.close();
			}

		}

	}

}
