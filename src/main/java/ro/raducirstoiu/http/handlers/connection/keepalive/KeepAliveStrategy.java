package ro.raducirstoiu.http.handlers.connection.keepalive;

import ro.raducirstoiu.http.connection.Connection;
import ro.raducirstoiu.http.request.Request;
import ro.raducirstoiu.http.response.Response;

public interface KeepAliveStrategy {
	
	/**
	 * read what the client wants in spite of Http 1.0 vs 1.1 default keep alive option
	 * @param request
	 */
	void readRequestPreference(Request request);
	
	/**
	 * inform about keep alive server decision (does not cover errors, but errors will override keep alive behavior)
	 * @param response
	 */
	void informResponseOfKeepAliveBehavior(Response response);
	
	/**
	 * 
	 * @param connection
	 * @return if the connection was closed or not
	 */
	boolean maybeCloseConnection(Connection connection);

	boolean isKeepAlive();

}
