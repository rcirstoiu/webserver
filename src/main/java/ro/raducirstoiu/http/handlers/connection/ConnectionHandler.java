package ro.raducirstoiu.http.handlers.connection;

/**
 * Connection handlers will be submitted to an Executor service, hence must implement Runnable
 * otherwise this is a marker interface for now
 * @author radu
 *
 */
public interface ConnectionHandler extends Runnable{

}
