package ro.raducirstoiu.http.handlers.connection.keepalive;

import ro.raducirstoiu.http.HeaderValues;
import ro.raducirstoiu.http.Headers;
import ro.raducirstoiu.http.request.Request;

public class Http11KeepAliveStrategy extends HttpXKeepAliveTemplate {

	private boolean keepAlive = true;
	@Override
	public void readRequestPreference(Request request) {
		if(request == null || !request.isInitialized()){
			throw new IllegalArgumentException("Request null or not yet initialized");
		}
		if(HeaderValues.CONNECTION_CLOSE.equals(request.getHeader(Headers.CONNECTION))){
			keepAlive = false;
		}
		
	}

	@Override
	public boolean isKeepAlive() {
		return keepAlive;
	}


}
