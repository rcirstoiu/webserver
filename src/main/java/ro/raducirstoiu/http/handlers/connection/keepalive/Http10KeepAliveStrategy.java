package ro.raducirstoiu.http.handlers.connection.keepalive;

import ro.raducirstoiu.http.HeaderValues;
import ro.raducirstoiu.http.Headers;
import ro.raducirstoiu.http.connection.Connection;
import ro.raducirstoiu.http.request.Request;
import ro.raducirstoiu.http.response.Response;

/**
 * in http 1.0 connections should be closed unless otherwise specified
 * (Conenction: keep-alive header is not actually part of the standard)
 * 
 * @author radu
 * 
 */
public class Http10KeepAliveStrategy extends HttpXKeepAliveTemplate {

	private boolean keepAlive = false;
	@Override
	public void readRequestPreference(Request request) {
		if(request == null || !request.isInitialized()){
			throw new IllegalArgumentException("Request null or not yet initialized");
		}
		if(HeaderValues.CONNECTION_KEEP_ALIVE.equals(request.getHeader(Headers.CONNECTION))){
			keepAlive = true;
		}

	}
	@Override
	public boolean isKeepAlive() {
		return keepAlive;
	}



}
