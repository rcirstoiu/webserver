package ro.raducirstoiu.http.handlers.request.staticcontent;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.raducirstoiu.http.RequestProcessingException;
import ro.raducirstoiu.http.handlers.request.RequestHandler;
import ro.raducirstoiu.http.request.Request;
import ro.raducirstoiu.http.resource.Resource;
import ro.raducirstoiu.http.resource.ResourceNotFoundNullObjects;
import ro.raducirstoiu.http.resource.ResourceResolver;
import ro.raducirstoiu.http.response.Response;
import ro.raducirstoiu.http.response.StatusCode;

/**
 * serves static content (i.e. the core assignment :) )
 * @author radu
 *
 */
public class StaticContentHandler implements RequestHandler {
	
	private static final Logger logger = LoggerFactory
			.getLogger(StaticContentHandler.class.getSimpleName());
	
	private final List<ResourceResolver> resourceResolvers = new ArrayList<ResourceResolver>();

	public StaticContentHandler(List<ResourceResolver> resourceResolvers){
		this.resourceResolvers.addAll(resourceResolvers);
	}
	
	@Override
	public boolean handle(Request request, Response response) {

		for(ResourceResolver resourceResolver : resourceResolvers){
			Resource resource = resourceResolver.getResource(request.getRequestURI());
			if(resource == ResourceNotFoundNullObjects.OUT_OF_SCOPE){
				continue;
			}else if (resource == ResourceNotFoundNullObjects.DOES_NOT_EXIST) {
				logger.trace("Resource not found:  {}",request.getRequestURI());
				return false;
			}
			else if(resource != null){
				
				logger.debug("Serving {} ({} bytes)",request.getRequestURI(), resource.getBytesCount());
				response.setContentType(resource.getContentType());
				response.setContentLength(resource.getBytesCount());
				response.setResponseStatus(StatusCode.STATUS_200);
				
				InputStream resourceInputStream = null;
				try {
					resourceInputStream = resource.getResourceInputStream();
					response.commit();
					IOUtils.copyLarge(resourceInputStream, response.getOutputStream());
				} catch (IOException e) {
					new ro.raducirstoiu.http.RequestProcessingException("Exceptions occured while pumping resource", e);
				}finally{
					if(resourceInputStream != null){
						try {
							resourceInputStream.close();
						} catch (IOException e) {
							throw new RequestProcessingException("Could not close resource stream", e);
						}
					}
					
				}
				return true;
			}
		}
		return false;
	}

}
