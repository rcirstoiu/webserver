package ro.raducirstoiu.http.handlers.request;

import ro.raducirstoiu.http.request.Request;
import ro.raducirstoiu.http.response.Response;

/**
 * Handles (http) requests once the request object was constructed
 * A request handler should be used as part of a request handler chain
 * @author radu
 *
 */
public interface RequestHandler {
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @return returns true if this handler handled the request, false otherwise
	 */
	boolean handle(Request request, Response response);

}
