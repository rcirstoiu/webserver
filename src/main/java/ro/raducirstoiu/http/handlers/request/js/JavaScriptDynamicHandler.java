package ro.raducirstoiu.http.handlers.request.js;

import ro.raducirstoiu.http.handlers.request.RequestHandler;
import ro.raducirstoiu.http.request.Request;
import ro.raducirstoiu.http.response.Response;

/**
 * Loads *.js files using Rhino; bootstraps js scripts with request and response objects
 * mainly used to demonstrate the flexibility of the approach (i.e. the approach is flexible
 * enough to handle static content as well as dynamic content)
 * @author radu
 *
 */
public class JavaScriptDynamicHandler implements RequestHandler{

	@Override
	public boolean handle(Request request, Response response) {
		// TODO Auto-generated method stub
		return false;
	}

}
