package ro.raducirstoiu.http.handlers.request;

import java.io.IOException;

import ro.raducirstoiu.http.RequestProcessingException;
import ro.raducirstoiu.http.request.Request;
import ro.raducirstoiu.http.response.Response;
import ro.raducirstoiu.http.response.StatusCode;

public class NotFoundHandler implements RequestHandler {

	private final static byte[] RESPONSE_BYTES = "<html><head> <title>Error 404 (Not found) !!! </title></head><body><h1>Resource not found<h1></body></html>"
			.getBytes();

	@Override
	public boolean handle(Request request, Response response) {
		response.setContentType("text/html; charset=UTF-8");
		response.setContentLength(RESPONSE_BYTES.length);
		response.setResponseStatus(StatusCode.STATUS_404);
		try {
			response.commit();
			response.getOutputStream().write(RESPONSE_BYTES);

		} catch (IOException e) {
			throw new RequestProcessingException(
					"Errors while commiting response", e);
		}

		return true;
	}

}
