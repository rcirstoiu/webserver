package ro.raducirstoiu.http.handlers.request.error;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import ro.raducirstoiu.http.ContentTypes;
import ro.raducirstoiu.http.RequestProcessingException;
import ro.raducirstoiu.http.handlers.request.RequestHandler;
import ro.raducirstoiu.http.request.Request;
import ro.raducirstoiu.http.response.Response;
import ro.raducirstoiu.http.response.StatusCode;

public class ExceptionHandler implements RequestHandler {

	private final Throwable t;

	public ExceptionHandler(Throwable t) {
		this.t = t;
	}

	@Override
	public boolean handle(Request request, Response response) {

		if (response != null && !response.isCommitted()) {
			response.setResponseStatus(StatusCode.STATUS_500);
			response.setContentType(ContentTypes.TEXT_PLAIN);
			byte[] exceptionTrace = exceptionToText(t);
			response.setContentLength(exceptionTrace.length);

			response.commit();
			try {
				response.getOutputStream().write(exceptionTrace);
			} catch (IOException e) {
				throw new RequestProcessingException(
						"Could not write exception stack trace to response stream");
			}

			return true;
		} else {
			return false;
		}

	}

	private byte[] exceptionToText(Throwable t) {
		ByteArrayOutputStream errorBytes = new ByteArrayOutputStream();
		PrintWriter printWriter = new PrintWriter(errorBytes);
		t.printStackTrace(printWriter);

		printWriter.close();

		return errorBytes.toByteArray();

	}

}
