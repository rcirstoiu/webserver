package ro.raducirstoiu.http;

public class RequestProcessingException extends RuntimeException{
	private static final long serialVersionUID = -7138329888133004109L;

	public RequestProcessingException(String reason,Exception ex) {
		super(reason,ex);
	}

	public RequestProcessingException(String reason) {
		super(reason);
	}


}
