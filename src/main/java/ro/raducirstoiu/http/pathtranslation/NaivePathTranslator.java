package ro.raducirstoiu.http.pathtranslation;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * translates between a path on a host to a disc path; N.B. the NAIVE part this
 * has no protection against path traversal or the such
 * 
 * @author radu
 * 
 */
public class NaivePathTranslator implements PathTranslation {

	private static final Logger logger = LoggerFactory
			.getLogger(NaivePathTranslator.class.getSimpleName());

	private final File rootFolder;
	private final String mappedAs;

	public NaivePathTranslator(File rootFolder, String mappedAs) {
		if (rootFolder == null || !rootFolder.isDirectory()) {
			throw new IllegalArgumentException("Not a folder: " + rootFolder);
		}

		if (mappedAs == null || "".equals(mappedAs)) {
			throw new IllegalArgumentException(
					"mappedAs cannot be null or empty");
		}
		this.rootFolder = rootFolder;
		this.mappedAs = mappedAs;
	}

	@Override
	public boolean knowsAboutPath(String path) {
		if (path != null) {
			return path.startsWith(mappedAs);
		} else {
			return false;
		}
	}
	@Override
	public String translatePath(String path) {
		logger.warn("will NAIVELY translate path: " + path);
		return rootFolder.getAbsolutePath()+ File.separatorChar + path.replaceFirst(mappedAs, "");
	}

}
