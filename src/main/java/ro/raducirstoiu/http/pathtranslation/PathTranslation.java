package ro.raducirstoiu.http.pathtranslation;

/**
 * Responsible for translating a requested path to a disk path
 * @author radu
 *
 */
public interface PathTranslation {

	public abstract boolean knowsAboutPath(String path);

	public abstract String translatePath(String path);

}