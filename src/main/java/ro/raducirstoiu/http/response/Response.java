package ro.raducirstoiu.http.response;

import java.io.OutputStream;

import ro.raducirstoiu.http.server.HttpVersion;

public interface Response {

	void setHeader(String header, String content);

	void setResponseStatus(StatusCode responseStatus);

	void setContentType(String contentType);

	void setContentLength(long contentLength);

	boolean isCommitted();

	void commit();

	OutputStream getOutputStream();

}
