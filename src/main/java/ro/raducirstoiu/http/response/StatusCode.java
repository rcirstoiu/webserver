package ro.raducirstoiu.http.response;

public enum StatusCode {
	
	STATUS_200 {
		@Override
		public int code() {
			return 200;
		}
		@Override 
		public String toString() {return "200 OK";}
	},
	STATUS_404 {
		@Override
		public int code() {
			return 404;
		}
		@Override 
		public String toString() {return "404 Not Found";}
	}, STATUS_500{
		@Override
		public int code() {
			return 500;
		}
		@Override 
		public String toString() {return "500 Internal Server Error";}
	}
	;
	
	
	public abstract int code();

}
