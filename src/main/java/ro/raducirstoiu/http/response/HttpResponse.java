package ro.raducirstoiu.http.response;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import ro.raducirstoiu.http.RequestProcessingException;
import ro.raducirstoiu.http.server.HttpVersion;

public class HttpResponse implements Response {

	private HttpVersion protocolVersion;
	private StatusCode responseStatus;
	private boolean committed = false;
	private OutputStream outputStream;

	private Map<String, String> headers = new HashMap<String, String>();

	public HttpResponse(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

	@Override
	public void setHeader(String header, String content) {
		headers.put(header, content);
	}

	public String getProtocolVersion() {
		if(protocolVersion == null){
			throw new IllegalStateException("Protocol version not defined");
		}
		return protocolVersion.toString();
	}

	public void setProtocolVersion(HttpVersion httpVersion) {
		this.protocolVersion = httpVersion;
	}

	public StatusCode getResponseStatus() {
		return responseStatus;
	}

	@Override
	public void setResponseStatus(StatusCode responseStatus) {
		this.responseStatus = responseStatus;
	}

	@Override
	public void setContentType(String contentType) {
		setHeader("Content-Type", contentType);
	}

	@Override
	public void setContentLength(long contentLength) {
		setHeader("Content-Length", "" + contentLength);
	}

	@Override
	public boolean isCommitted() {
		return committed;
	}

	@Override
	public void commit(){
		committed = true;
		StringBuilder statusAndHeaders = new StringBuilder();

		statusAndHeaders.append(getProtocolVersion()).append(" ")
				.append(responseStatus.toString()).append("\n");

		for (String header : headers.keySet()) {
			statusAndHeaders.append(header).append(": ")
					.append(headers.get(header)).append("\n");
		}

		// done writing headers
		statusAndHeaders.append("\r\n");
		try {
			outputStream.write(statusAndHeaders.toString().getBytes());
		} catch (IOException e) {
			throw new RequestProcessingException("Could not write to response stream", e);
		}

	}

	@Override
	public OutputStream getOutputStream() {
		return outputStream;
	}

	public void setOutputStream(OutputStream outputStream) {

	}

}
