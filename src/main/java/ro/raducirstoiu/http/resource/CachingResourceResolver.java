package ro.raducirstoiu.http.resource;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ro.raducirstoiu.http.RequestProcessingException;

/**
 * Naive caching of resources (note it has no cache evacuation so it will leak
 * memory) Its purpose is to demonstrate the power of the ResourceResolver
 * interface
 * 
 * @author radu
 * 
 */
public class CachingResourceResolver implements ResourceResolver {

	private static class CachedResource implements Resource {

		private final String contentType;
		private final long contentLenght;
		private final byte[] contents ;

		public CachedResource(Resource nonCachedResource) {
			if (nonCachedResource == null) {
				throw new IllegalArgumentException(
						"Cannot cache a null resource");
			}
			this.contentType = nonCachedResource.getContentType();
			this.contentLenght = nonCachedResource.getBytesCount();

			this.contents = new byte[(int) contentLenght];
			BufferedInputStream bufferedStream = null;
			try {
				bufferedStream = new BufferedInputStream(
						nonCachedResource.getResourceInputStream());
				nonCachedResource.getResourceInputStream().read(contents);
			} catch (IOException e) {
				throw new RequestProcessingException(
						"Exceptions occured while trying to read cachig content",
						e);
			} finally {
				if (bufferedStream != null) {
					try {
						bufferedStream.close();
					} catch (IOException e) {
						throw new RequestProcessingException(
								"Could not close stream", e);
					}
				}
			}
		}

		@Override
		public InputStream getResourceInputStream() throws IOException {
			return new ByteArrayInputStream(contents);
		}

		@Override
		public long getBytesCount() {
			return contentLenght;
		}

		@Override
		public String getContentType() {
			return contentType;
		}

	}

	private final ResourceResolver wrappedResolver;
	private final ConcurrentHashMap<String, Resource> resourceCache = new ConcurrentHashMap<String, Resource>();

	public CachingResourceResolver(ResourceResolver wrappedResolver) {
		this.wrappedResolver = wrappedResolver;
	}

	@Override
	public Resource getResource(String path) {
		if (resourceCache.contains(path)) {
			return resourceCache.get(path);
		} else {
			Resource nonCachedResource = wrappedResolver.getResource(path);
			return resourceCache.putIfAbsent(path, new CachedResource(nonCachedResource));
		}
	}

}
