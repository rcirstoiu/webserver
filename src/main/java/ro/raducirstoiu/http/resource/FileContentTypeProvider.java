package ro.raducirstoiu.http.resource;

import java.io.File;

import static ro.raducirstoiu.http.ContentTypes.*;

public class FileContentTypeProvider {

	public String getContentType(File file){
		if(file == null || ! file.isFile()){
			throw new IllegalArgumentException("Cannot get content type for: " + file);
		}
		String fileName = file.getName();
		
		if(fileName.endsWith(".pdf")){
			return APPLICATION_PDF;
		} else if(fileName.endsWith(".txt")){
			return TEXT_PLAIN;
		} else if(fileName.endsWith(".html")){
			return TEXT_HTML;
		}
		
		return APPLICATION_OCTETSTREAM;
	}
}
