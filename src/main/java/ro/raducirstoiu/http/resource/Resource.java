package ro.raducirstoiu.http.resource;

import java.io.IOException;
import java.io.InputStream;

/**
 * interface for a resource that is to be served
 * @author radu
 *
 */
public interface Resource {
	
	public InputStream getResourceInputStream() throws IOException;
	public long getBytesCount();
	public String getContentType();

}
