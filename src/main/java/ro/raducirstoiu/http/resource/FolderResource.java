package ro.raducirstoiu.http.resource;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import ro.raducirstoiu.http.ContentTypes;

public class FolderResource implements Resource {

	private final File file;
	private byte[] folderRepresentationBytes;

	public FolderResource(File file) {
		if (file == null || !file.isDirectory()) {
			throw new IllegalArgumentException("File: " + file
					+ " is null or not a directory");
		}
		this.file = file;
	}

	@Override
	public InputStream getResourceInputStream() throws IOException {
		if (folderRepresentationBytes == null) {
			createFolderRepresentation();
		}

		return new ByteArrayInputStream(folderRepresentationBytes);
	}

	private void createFolderRepresentation() {
		String[] folderContents = file.list();

		StringBuffer sb = new StringBuffer();
		sb.append(file.getName()).append(":\n");

		if (folderContents != null) {
			for (String child : folderContents) {
				sb.append(child).append("\n");
			}
		}
		folderRepresentationBytes = sb.toString().getBytes();
	}

	@Override
	public long getBytesCount() {
		if (folderRepresentationBytes == null) {
			createFolderRepresentation();
		}

		return folderRepresentationBytes.length;
	}

	@Override
	public String getContentType() {
		return ContentTypes.TEXT_PLAIN;
	}

}
