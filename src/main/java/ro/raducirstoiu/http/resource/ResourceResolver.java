package ro.raducirstoiu.http.resource;

/**
 * finds a resource given its name
 * @author radu
 *
 */
public interface ResourceResolver {
	
	Resource getResource(String path);

}
