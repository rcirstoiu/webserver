package ro.raducirstoiu.http.resource;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


public class StaticFileResource implements Resource{
	
	private final File file;

	public StaticFileResource(File file){
		this.file = file;
	}

	@Override
	public InputStream getResourceInputStream() throws IOException {
		return new FileInputStream(file);
	}

	@Override
	public long getBytesCount() {
		return file.length();
	}

	private final FileContentTypeProvider contentTypeProvider = new FileContentTypeProvider();
	@Override
	public String getContentType() {
		return contentTypeProvider.getContentType(file);
	}

}
