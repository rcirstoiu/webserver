package ro.raducirstoiu.http.resource;

import java.io.File;

import ro.raducirstoiu.http.pathtranslation.NaivePathTranslator;
import ro.raducirstoiu.http.pathtranslation.PathTranslation;

public class StaticResourceResolver implements ResourceResolver {
	
	private final PathTranslation pathTranslator;

	public StaticResourceResolver(File rootFolder, String mappedAs){
		pathTranslator = new NaivePathTranslator(rootFolder, mappedAs);
	}
	
	@Override
	public Resource getResource(String path) {
		
		if(!pathTranslator.knowsAboutPath(path)){
			return ResourceNotFoundNullObjects.OUT_OF_SCOPE;
		}
		
		String translatedPath = pathTranslator.translatePath(path);
		File file = new File(translatedPath);
		
		if(!file.exists() ){
			return ResourceNotFoundNullObjects.DOES_NOT_EXIST;
		}else if (file.isDirectory()){
			return new FolderResource(file);
		}
		
		return new StaticFileResource(file);
	}


}
