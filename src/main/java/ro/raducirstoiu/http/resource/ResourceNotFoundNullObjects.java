package ro.raducirstoiu.http.resource;

import java.io.InputStream;

/**
 * A resource was not found, but I want to convey more meaning about the reason it was not found
 * enter the Null Object
 * @author radu
 *
 */
public class ResourceNotFoundNullObjects {
	
	private static class NonExistingResource implements Resource {

		@Override
		public InputStream getResourceInputStream() {
			throw new IllegalStateException("This is a null object, deal with it but don't call its methods ....");
		}

		@Override
		public long getBytesCount() {
			throw new IllegalStateException("This is a null object, deal with it but don't call its methods ....");
		}

		@Override
		public String getContentType() {
			throw new IllegalStateException("This is a null object, deal with it but don't call its methods ....");
		}
	}
	
	//path to resource was correct and the resource simply does note exist at this path
	public final static Resource DOES_NOT_EXIST = new NonExistingResource();
	//resource was not found because the path was out of scope (i.e. another resource resolver might find it)
	public final static Resource OUT_OF_SCOPE = new NonExistingResource();

}
