package ro.raducirstoiu.http.request;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.raducirstoiu.http.Headers;
import ro.raducirstoiu.http.RequestProcessingException;

public class HttpRequest implements Request {

	private static final Logger logger = LoggerFactory
			.getLogger(HttpRequest.class.getSimpleName());

	private String requestURI;
	private String requestQuery;
	private String protocolVersion;
	private String protocol;
	private String method;
	private String requestBody;
	Map<String, String> headers = new HashMap<String, String>();

	private boolean initialized;

	@Override
	public String getMethod() {
		return method;
	}

	@Override
	public String getHostName() {
		String hostAndPort = getHeader(Headers.HOST);
		if (hostAndPort != null) {
			return hostAndPort.split(":")[0];
		}

		return null;
	}

	@Override
	public String getProtocol() {
		return protocol;
	}

	@Override
	public String getProtocolVersion() {
		return protocolVersion;
	}

	@Override
	public String getRequestURI() {
		return requestURI;
	}

	public String getRequestBody() {
		return requestBody;
	}

	@Override
	public String getHeader(String headerName) {
		return headers.get(headerName);
	}

	@Override
	public void build(InputStream inputStream) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));

		logger.trace("reading request line");
		String requestLine = reader.readLine();
		logger.debug("Request {}", requestLine);

		if (requestLine == null) {
			throw new RequestProcessingException("Could not read request line");
		}

		parseRequestLine(requestLine);

		String headerLine = null;
		while ((headerLine = reader.readLine()) != null
				&& !"".equals(headerLine)) {
			parseHeader(headerLine);
		}
		if (headerLine == null) {
			throw new IllegalArgumentException(
					"Malformed HTTP request, CRLF should separate request from request body");
		}

		if (getHeader(Headers.CONTENT_LENGTH) != null) {
			requestBody = reader.readLine();
		}
		initialized = true;

	}

	private final static Pattern REQ_LINE_PATTERN = Pattern
			.compile("([A-Z]+) ([^ ]+) ([A-Z]+)/(.+)");
	private final static Pattern HEADER_PATTERN = Pattern
			.compile("([^:]+)[:] (.+)");

	void parseRequestLine(String requestLine) {
		Matcher matcher = REQ_LINE_PATTERN.matcher(requestLine);
		if (matcher.matches()) {
			method = matcher.group(1);
			extractFixedResourceAndQuery(matcher.group(2));
			protocol = matcher.group(3);
			protocolVersion = matcher.group(4);

		} else {
			throw new IllegalArgumentException("Invalid request argument: "
					+ requestLine);
		}

	}

	/**
	 * extract the fixed resource request and the query part of a request line
	 * 
	 * @param completeRequestLine
	 */
	private void extractFixedResourceAndQuery(String completeRequestLine) {
		if (completeRequestLine != null) {
			int questionMarkIndex = completeRequestLine.indexOf('?');
			if (questionMarkIndex > 0) {
				requestURI = completeRequestLine
						.substring(0, questionMarkIndex);
				requestQuery = completeRequestLine.substring(
						questionMarkIndex + 1, completeRequestLine.length());
			} else {
				requestURI = completeRequestLine;
			}
		}
	}

	private void parseHeader(String headerLine) {
		Matcher matcher = HEADER_PATTERN.matcher(headerLine);
		if (matcher.matches()) {
			String headerName = matcher.group(1);
			String headerValue = matcher.group(2).trim();
			headers.put(headerName, headerValue);
		} else {
			throw new IllegalArgumentException("Malformed header: "
					+ headerLine);
		}
	}

	@Override
	public String getRequestQuery() {
		return requestQuery;
	}

	@Override
	public boolean isInitialized() {
		return initialized;
	}

}
