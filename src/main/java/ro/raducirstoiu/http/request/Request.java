package ro.raducirstoiu.http.request;

import java.io.IOException;
import java.io.InputStream;

public interface Request {
	String getMethod();
	String getHostName();
	String getProtocol();
	String getProtocolVersion();
	String getRequestURI();
	String getRequestQuery();
	String getHeader(String headerName);
	void build(InputStream inputStream) throws IOException;
	boolean isInitialized();

}
