package ro.raducirstoiu.http.connection;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Abstracts away the transport (http/https); 
 * Also keeps connection specific properties (such as keep-alive)
 * @author radu
 *
 */
public interface Connection {
	Socket getSocket();

	InputStream getInputStream() throws IOException;

	OutputStream getOutputStream() throws IOException; 
	
	void setKeepAlive(boolean keepAlive);
	
	boolean getKeepAlive();
	
	public void close();
	
	public boolean isClosed();
	
	//TODO: is this a good place for sessions?

}
