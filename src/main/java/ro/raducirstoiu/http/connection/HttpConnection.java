package ro.raducirstoiu.http.connection;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author radu
 *
 */
public class HttpConnection implements Connection {
	private static final Logger logger = LoggerFactory
			.getLogger(HttpConnection.class.getSimpleName());

	private final Socket socket;
	public HttpConnection(Socket socket) {
		this.socket = socket;
	}

	@Override
	public Socket getSocket() {
		return socket;
	}

	@Override
	public InputStream getInputStream()
            throws IOException{
		return socket.getInputStream();
	}
	
	@Override
	public OutputStream getOutputStream() throws IOException {
		return socket.getOutputStream();
	}

	boolean keepAlive = false;
	@Override
	public void setKeepAlive(boolean keepAlive) {
		this.keepAlive = keepAlive;
	}

	@Override
	public boolean getKeepAlive() {
		return keepAlive;
	}

	@Override
	public void close() {
		logger.debug("closing connection");
		if(socket != null)
		{
			try {
				socket.close();
			} catch (IOException e) {
				throw new CannotCloseConnectionException();
			}
		}
		
	}

	@Override
	public boolean isClosed() {
		return socket.isClosed();
		
	}

}
