package ro.raducirstoiu.http;

public class ContentTypes {
	public final static String TEXT_HTML= "text/html";
	public final static String TEXT_PLAIN = "text/plain";
	public final static String APPLICATION_PDF = "application/pdf";
	public final static String  APPLICATION_OCTETSTREAM = "application/octet-stream";

}
