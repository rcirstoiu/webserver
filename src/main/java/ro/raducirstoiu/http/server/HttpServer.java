package ro.raducirstoiu.http.server;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.raducirstoiu.http.handlers.request.NotFoundHandler;
import ro.raducirstoiu.http.handlers.request.staticcontent.StaticContentHandler;
import ro.raducirstoiu.http.resource.ResourceResolver;
import ro.raducirstoiu.http.resource.StaticResourceResolver;

/**
 * General server management such as starting/stopping accepts new connections
 * 
 */
public class HttpServer {
	private static final Logger logger = LoggerFactory
			.getLogger(HttpServer.class.getSimpleName());
	
	
	private int port = 8080;
	private final ExecutorService threadPool;

	private ServerSocket serverSocket;


	private AcceptConnections acceptConnectionsThread;


	private Map<String, VHostConfig> vhostConfig = new HashMap<String,VHostConfig>();
	
	public HttpServer() {
		threadPool = Executors.newCachedThreadPool();

	}
	
	public void addVhost(VHostConfig vhost){
		vhostConfig.put(vhost.getVhostName(), vhost);
	}
	
	public Map<String,VHostConfig> getVhostConfig() {
		
		return vhostConfig;
		
	}

	private boolean started = false;
	public synchronized void start() throws IOException {
		if(started) {
			throw new IllegalStateException("Server already started");
		}
		logger.info("starting server");
		serverSocket = new ServerSocket(port);
		
		acceptConnectionsThread = new AcceptConnections(this,serverSocket);
		acceptConnectionsThread.start();

	}

	/**
	 * 
	 * @param nicely -- whether to wait for current requests to finish or not (force close)
	 */
	public synchronized void stop(boolean nicely) {
		logger.info("Shutting down " + (nicely ? "nicely" : "hard"));
		
		acceptConnectionsThread.stopAccepting();

		if(nicely){
			threadPool.shutdown();
		}else {
			threadPool.shutdownNow();
		}
		
		try {
			serverSocket.close();
		} catch (IOException e) {
			logger.error("Cannot close server socket" , e);
		}finally {
			started = false;
		}
		
		//wait for the accepting thread to die
		try {
			acceptConnectionsThread.join();
		} catch (InterruptedException e) {
		} 

	}

	public HttpServer setPort(int port) {
		this.port = port;
		return this;
	}
	
	ExecutorService getThreadPool() {
		return threadPool;
	}

	public static void main(String[] args) throws IOException {
		
		
		VHostConfig localhost = new VHostConfig("localhost");
		localhost.setHttpVersion(HttpVersion.HTPP_11);
		ResourceResolver resolver = new StaticResourceResolver(new File("./localhost"), "/");
		localhost.addResourceResolver(resolver);
		
		localhost.addRequestHandler(new StaticContentHandler(localhost.getResourceResolvers()));
		localhost.addRequestHandler(new NotFoundHandler());
		
		final HttpServer server = new HttpServer();
		server.addVhost(localhost);
		server.start();
		
	}

}
