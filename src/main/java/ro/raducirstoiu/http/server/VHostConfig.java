package ro.raducirstoiu.http.server;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import ro.raducirstoiu.http.handlers.request.RequestHandler;
import ro.raducirstoiu.http.resource.ResourceResolver;

/**
 * Stores configuration for a certain virtual host
 * @author radu
 *
 */
public class VHostConfig {
	
	private final List<RequestHandler> requestHandlers = new LinkedList<RequestHandler>();
	private final List<ResourceResolver> resourceResolvers = new LinkedList<ResourceResolver>();
	private final String vhostName;
	private HttpVersion httpVersion;
	
	public VHostConfig(String vhostName){
		if(vhostName == null || "".equals(vhostName))
			throw new IllegalArgumentException("invalid virtual host name: "+vhostName);
		this.vhostName = vhostName;
	}
	
	public VHostConfig addRequestHandler(RequestHandler requestHandler){
		requestHandlers.add(requestHandler);
		return this;
	}
	
	public VHostConfig addResourceResolver(ResourceResolver resourceResolver){
		resourceResolvers.add(resourceResolver);
		return this;
	}
	
	public List<RequestHandler> getRequestHandlers() {
		List<RequestHandler> ret = new ArrayList<RequestHandler>(requestHandlers.size());
		ret.addAll(requestHandlers);
		return ret;
	}
	
	public List<ResourceResolver> getResourceResolvers(){
		List<ResourceResolver> ret = new ArrayList<ResourceResolver>(resourceResolvers.size());
		ret.addAll(resourceResolvers);
		return ret;
	}

	public String getVhostName() {
		return vhostName;
	}

	public HttpVersion getHttpVersion() {
		return httpVersion;
	}

	public void setHttpVersion(HttpVersion htpp11) {
		this.httpVersion = htpp11;
	}
	
}
