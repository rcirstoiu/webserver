package ro.raducirstoiu.http.server;

import ro.raducirstoiu.http.handlers.connection.keepalive.Http10KeepAliveStrategy;
import ro.raducirstoiu.http.handlers.connection.keepalive.Http11KeepAliveStrategy;
import ro.raducirstoiu.http.handlers.connection.keepalive.KeepAliveStrategy;

public enum HttpVersion {
	HTPP_11 {
		public String toString() {
			return "HTTP/1.1";
		}

		@Override
		public KeepAliveStrategy getKeepAliveStrategy() {
			return new Http11KeepAliveStrategy();
		}
	},
	HTPP_10 {
		public String toString() {
			return "HTTP/1.0";
		}

		@Override
		public KeepAliveStrategy getKeepAliveStrategy() {
			return new Http10KeepAliveStrategy();
		}
	};

	public abstract KeepAliveStrategy getKeepAliveStrategy();

}
