package ro.raducirstoiu.http.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.raducirstoiu.http.connection.HttpConnection;
import ro.raducirstoiu.http.handlers.connection.HttpConnectionHandler;

class AcceptConnections extends Thread {
	
	private static final Logger logger = LoggerFactory
			.getLogger(AcceptConnections.class.getSimpleName());
	
	private final ServerSocket serverSocket;
	private volatile boolean stopped = false;
	private HttpServer server;

	public AcceptConnections(HttpServer server,ServerSocket serverSocket) {
		this.server = server;
		this.serverSocket = serverSocket;
	}
	
	public void stopAccepting() {
		stopped = true;
		
	}
	public void run() {
		while(!stopped){
			try {
				logger.trace("Will call .accept (blocking untill request received)");
				
				Socket socket = serverSocket.accept();
				logger.debug("Accepted connection from {}", socket.getRemoteSocketAddress());
				HttpConnection connection =  new HttpConnection(socket);
				HttpConnectionHandler connectionHandler = new HttpConnectionHandler(connection,server.getVhostConfig());
				server.getThreadPool().execute(connectionHandler);
				logger.trace("Connectin moved to serve thread pool....");
			} catch (SocketException acceptInterrupted){
				if("Socket closed".equals(acceptInterrupted.getMessage())){
					logger.trace(".accept was interrupted because socket was closed");
				}
				else {
					logger.debug("Socket exception occured: ",acceptInterrupted);
				}
				
			} catch (IOException e) {
				logger.error("Cannot accept new connection", e);
				stopped = true;
			}
		}
	}
}