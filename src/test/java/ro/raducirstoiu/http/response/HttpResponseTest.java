package ro.raducirstoiu.http.response;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.junit.Test;

import ro.raducirstoiu.http.server.HttpVersion;

public class HttpResponseTest {
	
	@Test
	public void testStatusAndHeaders() throws IOException{
		
		ByteArrayOutputStream mockStream = new ByteArrayOutputStream();
		HttpResponse response = new HttpResponse(mockStream);
		
		response.setResponseStatus(StatusCode.STATUS_200);
		response.setContentLength(1);
		response.setContentType("application/json");
		response.setProtocolVersion(HttpVersion.HTPP_11);
		
		response.commit();
		
		String expectedConten = "HTTP/1.1 200 OK\n"
								+"Content-Length: 1\n"
								+"Content-Type: application/json\n" + "\r\n";
		
		assertEquals(expectedConten, mockStream.toString());
	}

}
