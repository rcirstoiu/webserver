package ro.raducirstoiu.http.request;
import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.junit.Test;

public class HttpRequestTest {
	private final static String CRLF = "\r\n";
	
	@Test
	public void testRequestLine() throws IOException {
		String rawRequest = "GET /index.html?param1=1 HTTP/1.1\n" 
							+"Host: www.example.com\n"
							+"Content-Type: application/x-www-form-urlencoded\n"
							+"Accept-Encoding: gzip,deflate,sdch\n"
							+"Accept-Language: en-US,en;q=0.8\n"
							+CRLF;
		InputStream requestInputStream = new ByteArrayInputStream(rawRequest.getBytes());
		HttpRequest httpRequest = new HttpRequest();
		
		httpRequest.build(requestInputStream);
		
		assertEquals("GET", httpRequest.getMethod());
		assertEquals("/index.html", httpRequest.getRequestURI());
		assertEquals("HTTP", httpRequest.getProtocol());
		assertEquals("1.1", httpRequest.getProtocolVersion());
		assertEquals("gzip,deflate,sdch", httpRequest.getHeader("Accept-Encoding"));
		assertEquals("param1=1",httpRequest.getRequestQuery());
	}

}
