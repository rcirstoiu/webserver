package ro.raducirstoiu;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import ro.raducirstoiu.http.handlers.request.NotFoundHandler;
import ro.raducirstoiu.http.handlers.request.staticcontent.StaticContentHandler;
import ro.raducirstoiu.http.resource.ResourceResolver;
import ro.raducirstoiu.http.resource.StaticResourceResolver;
import ro.raducirstoiu.http.server.HttpServer;
import ro.raducirstoiu.http.server.HttpVersion;
import ro.raducirstoiu.http.server.VHostConfig;

/**
 * integration test for downloading file
 * @author radu
 *
 */
public class DownloadTestIT {

	private final static String FOLDER_NAME = "target";
	
	private final static String FILE_NAME = "random_data_test";
	private final static String RELATIVE_FILE_NAME = FOLDER_NAME + File.separator
			+ FILE_NAME;
	
	private final static int FILE_SIZE = 10 * 1024 * 1024;// 10M
	private final static String V_HOST = "localhost";
	private final static int PORT = 12345;
	
	private static HttpServer server;
	
	@BeforeClass
	public static void startServer() throws IOException {
		
		VHostConfig localhost = new VHostConfig("localhost");
		localhost.setHttpVersion(HttpVersion.HTPP_11);
		ResourceResolver resolver = new StaticResourceResolver(new File(FOLDER_NAME), "/");
		localhost.addResourceResolver(resolver);
		localhost.addRequestHandler(new StaticContentHandler(localhost.getResourceResolvers()));
		localhost.addRequestHandler(new NotFoundHandler());
		
		server = new HttpServer();
		server.addVhost(localhost);
		server.setPort(PORT);
		server.start();
	}
	
	@AfterClass
	public static void stopServer() throws Exception {
		server.stop(false);
	}

	@BeforeClass
	public static void createRandomDataTestFile() throws IOException {
		OutputStream output = null;
		try {
			output = new BufferedOutputStream(new FileOutputStream(RELATIVE_FILE_NAME));

			byte[] randomData = new byte[FILE_SIZE];
			new Random().nextBytes(randomData);
			output.write(randomData);
		} finally {
			if (output != null) {
				output.close();
			}

		}
	}

	@Test
	public void testCorrectFileDownloaded() throws ClientProtocolException,
			IOException {

		BufferedInputStream actualFileStream = null;
		BufferedInputStream responseStream =null;
		try {
			HttpClient client = new DefaultHttpClient(); 
			HttpGet request = new HttpGet("http://" + V_HOST + ":" + PORT + "/"
					+ FILE_NAME);
			responseStream = new BufferedInputStream(client
					.execute(request).getEntity().getContent());
			actualFileStream = new BufferedInputStream(
					new FileInputStream(RELATIVE_FILE_NAME));

			assertEquals(
					"Direct file stream and http stream should be the same",
					true,
					InputStreamCompare.isSame(actualFileStream, responseStream));
		} finally {
			if(actualFileStream != null)
			{
				actualFileStream.close();
			}
		}
	}

}
