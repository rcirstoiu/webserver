How to start the server

A good example as to how to start the http server can be found in ro.raducirstoiu.DownloadTestIT which is an Integration Test that generates a 10MB file (random data) then
it writes it to disc, starts the http server, downloads the file and compares it with the original.
You can also run start-localhost.sh to start the server configured to listen on  http://localhost:8080 ( / is mapped in the "localhost" folder)
Or you could just run:

mvn exec:java -Dexec.mainClass="ro.raducirstoiu.http.server.HttpServer"

to accomplish the same thing




Notes:

- Regarding the thread-pooling part of the assignment -- the server uses the Executors.newCachedThreadPool (unbounded) mainly because I use the classic java io 
(had I known NIO I would have used a fixed thread pool)

- Configuration (except for listening port) is done per virtual host (a virtual host could be serving http 1.1 and another http 1.0 for instance)

- Keep Alive behavior is configurable using ro.raducirstoiu.http.handlers.connection.keepalive.KeepAliveStrategy with two concrete strategies (for HTTP 1.0 and HTTP 1.1)


Major parts:

ro.raducirstoiu.http.server.HttpServer -- mainly used starting and stopping an http server
ro.raducirstoiu.http.server.VHostConfig -- represents the configuration for a virtual host
ro.raducirstoiu.http.request.HttpRequest -- represents the http request (similar to HttpServletRequest)
ro.raducirstoiu.http.response.HttpResponse -- the http response (similar to HttpServletResponse)
ro.raducirstoiu.http.resource.Resource -- abstract resource (file/folder/ not found resource)
ro.raducirstoiu.http.resource.ResourceResolver -- a very nice (self proclaimed) interface for finding a resource; a virtual host is configured with a list of resource resolvers
						 which map a http requested resorurce to a concrete resource; because it is an interface its very easy to extend 
ro.raducirstoiu.http.handlers.request.RequestHandler  handles a HttpRequest and writes back to a HttpResponse; some concrete implementations: StaticContentHandler,
							NotFoundHandler, ExceptionHandler


Copied code:

- the only copied code is ro.raducirstoiu.InputStreamCompare

